# scripting



## cmn - CoMaNd

![cmn](https://bitbucket.org/phdieu/scripting/raw/23beade94716c01bd3548b5f407c45149b373079/img/1.PNG)

### Installation

For Windows

1. Put this script to %USERPROFILE%\bin

2. Modify WORKSPACE_DIR, ENGINE_DIR and ECLUDES based on your enviroments



### Essential Feature

1. Git
   - Checkout specific branch of all git project
   - Pull master branch of all git project
   - Clean git project
   - Check file history

2. Maven
   - Run test
   - Run install
3. Mock service
   - Start
   - Stop



### Ussage

Synchronize all cob modules in workspace

`$cmn cob sync`



Build all cob modules

`$cmn cob build`



Synchronize and build all cob modules inwork space

`$cmn cob sync && cmn cob build`



Delete all local branch which is merged 

`$cmn clean`



 Switch to a specific branch for all module in the workspace 

`$cmn cob checkout master`
`$cmn cob checkout cybertron/hot-release`



Start mock service

`$cmn mock start`