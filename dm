#!/bin/bash

# ---------------------------------------------------------------------------
# Ivy Engine Control Script
#
# A powerful command-line control script for servers.
# ---------------------------------------------------------------------------


# Get executable name
# ---------------------------------------------------------------------------
PROG=$(basename $0)
ENGINE_DIR=/opt/ivy_engine
ENGINE_NAME=AxonIvyEngine7.0.3_$1_${2^^}
ENGINE_SERVICE_NAME=$17.0.3_$2.service
BOOMERANG_DIR=/home/fintech/boomerang
INFO="INFO:"
ERROR="ERROR:"
WARNING="WARNING"
DEFAULT_LOG_LINES=1000

# Script Usage
# ---------------------------------------------------------------------------
usage() {
cat <<EOF
Ivy Engine Control Script
Usage: $PROG <bank> <environment> <action> [<options>]
       $PROG <service> <command> <configuration>

Banks:
  alpha               Alpha RHEINTAL Bank
  acrevis             Acrevis Bank

Services:
  boomerang           Mocks service using Sandbox API and Fronttail API.
                      https://bitbucket.org/aavn_nhduong/boomerang

Environments:
  t|T                 Test Server
  q|Q                 Qualify Server
  d|D                 Development Server

Actions:

  start [<option>]    Start the engine server.
  stop                Stop the engine server.
  force-stop          Forcibly stop the engine server.
  restart [<option>]  Restart the engine server.
  force-restart       Forcibly restart the engine server.
  list [<option>]     Display a list of servers.
  status              Display the status of the server.
  go                  Change the currrent directory to engine directory.
  log [<option>]      Display log file of server.

Commands:

  start [<option>]    Start the service.
  stop                Stop the service.
  restart [<option>]  Restart the service.
  list [<option>]     Display a list of services.
  status              Display the status of the service.
  go                  Change the currrent directory to service directory.
  log [<option>]      Display log file of service.
  install             Install the service.

Options:
  start|restart:
      Options:
        -l <number>   Display log of server.
  list:
      Options:
        -enabled      Display a list of enabled servers, default.
        -disabled     Display a list of disabled servers.
        -running      Display a list of running servers.
        -stopped      Display a list of stopped servers.
      If no option, all available servers are listed.
  log:
      Options:
        -l <number>   Display log file with <number> of lines.

Configurations:
  start|restart:
      Options:
        -p <port>     Set listening port, default is 9004.
        -x <port>     Set port to run as sandbox service, default is 9003.
        -l <lines>    Set Number on lines stored in browser, default 1500.
EOF
}

# ---------------------------------------------------------------------------
# Check to see if the engine directory is existed.
#
# @param 1 The engine.
# @param 2 The environment.
# @return A 0 if engine directory is existed, a 1 otherwise.
# ---------------------------------------------------------------------------
checkEngineDirectory() {
    if [ ! -d $ENGINE_DIR/$ENGINE_NAME ]; then
        printf "$ERROR Engine ${1^} ${2^^} not recognized.\n"
        return 1;
    fi
    return 0;
}

# ---------------------------------------------------------------------------
# Check to see if the engine service is registered.
#
# @param 1 The engine.
# @param 2 The environment.
# @return A 0 if engine directory is registered, a 1 otherwise.
# ---------------------------------------------------------------------------
checkServiceActive() {
    #isActive="$(systemctl list-units --type=service | awk '{ print $1 }' | grep .service | grep $ENGINE_SERVICE_NAME)";
    #if [ ! $isActive ]; then
    #    return 1;
    #fi
    return 0;
}

execute() {
    if ! checkServiceActive ${@}; then
        printf "$ERROR Engine service ${1^} ${2^^} not registered.\n"
        exit 0;
    fi
    sudo systemctl $3 $ENGINE_SERVICE_NAME
}

start() {
    execute ${@}
    [[ ! -z "$4" && $4 == "-l" ]] && (log ${@})
}

stop() {
    execute ${@}
    [[ ! -z "$4" && $4 == "-l" ]] && (log ${@})
}

force-stop() {
    printf "incomming"
}

restart() {
    execute ${@}
    [[ ! -z "$4" && $4 == "-l" ]] && (log ${@})
}

force-restart() {
    printf "incomming"
}

list() {
    printf "incomming"
}

status() {
    execute ${@}
}

go() {
    cd $ENGINE_DIR/$ENGINE_NAME
    exec bash
}

log() {
    lines=$DEFAULT_LOG_LINES;

    if [ ! -z $4 ] && [ $4 == "-l" ] && [ ! -z $5 ]; then
        lines=$5;
    fi

    tail -f -n $lines $ENGINE_DIR/$ENGINE_NAME/logs/ch.ivyteam.ivy.log
}

sgo() {
    cd $BOOMERANG_DIR
    exec bash
}

slog() {

}

engine() {
    case "$3" in
        start)
            e_start ${@}
            exit 0
            ;;
        stop)
            stop ${@}
            exit 0
            ;;
        force-stop)
            force-stop ${@}
            exit 0
            ;;
        restart)
            restart ${@}
            exit 0
            ;;
        force-restart)
            force-restart ${@}
            exit 0
            ;;
        list)
            list ${@}
            exit 0
            ;;
        status)
            status ${@}
            exit 0
            ;;
        go)
            go ${@}
            exit 0
            ;;
        log)
            log ${@}
            exit 0
            ;;
        *)
            #echo "Axon.ivy Engine project" $1 $server "action:" $3
            #sudo systemctl $3 $17.0.3_$2.service
            exit 0
            ;;
    esac
}

boomerang() {
    case "$2" in
        start)
            
            exit 0
            ;;
        stop)
            
            exit 0
            ;;
        restart)
            
            exit 0
            ;;
        list)
            
            exit 0
            ;;
        status)
            
            exit 0
            ;;
        go)
            sgo ${@}
            exit 0
            ;;
        log)
            slog 
            exit 0
            ;;
        *)
            exit 0
            ;;
    esac
}

# Respond to the command line arguments.
# ---------------------------------------------------------------------------
case "$1" in
    -h | --help | -help | help | -usage | usage)
        usage
        exit 0
        ;;
    acrevis | alpha)
        if checkEngineDirectory ${@}; then
            engine ${@}
        fi
        run ${@}
        exit 0
        ;;
    boomerang)
        boomerang ${@}
        exit 0
        ;;
    *)
        usage
        exit 0
        ;;
esac